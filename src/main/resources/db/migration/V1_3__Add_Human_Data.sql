SET autocommit = OFF;
START TRANSACTION;
    INSERT INTO M_HUMAN (name, age, gender) VALUES
        ('Maverick', '24', 'M'),
        ('Pojo', '30', 'M'),
        ('Diana', '25', 'F');
COMMIT;